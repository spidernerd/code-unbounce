  //Unbounce inline content lightbox v2.0
  //ubContentLightbox('boxContainingForm', 'buttonToTriggerLightbox');

  //boxContainingForm is the ID of the box that contains your form
  //buttonToTriggerLightbox is the ID of the button that will trigger the form lightbox

  ubContentLightbox('#lp-pom-box-150', '#lp-pom-button-37');

  document.write('<style type="text/css"> #fancybox-outer {-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"}</style>');

  function ubContentLightbox(boxContainingForm, buttonToTriggerForm) {
      document.write('<style type="text/css">' + boxContainingForm + '{display:none}</style>');
      $(function() {
          var boxWidth = $(boxContainingForm).width();
          var boxHeight = $(boxContainingForm).height();
          var borderWidthTop = $(boxContainingForm).css('border-top-width');
          var borderWidthBottom = $(boxContainingForm).css('border-bottom-width');
          var borderWidthLeft = $(boxContainingForm).css('border-left-width');
          var borderWidthRight = $(boxContainingForm).css('border-right-width');
          var borderRadius = $(boxContainingForm).css('border-top-left-radius');
          var borderHeight = parseFloat(borderWidthTop) + parseFloat(borderWidthBottom);
          var borderWidth = parseFloat(borderWidthLeft) + parseFloat(borderWidthRight);
          var borderRadiusValue = parseFloat(borderRadius);
          boxWidth = parseFloat(boxWidth) + borderWidth;
          boxHeight = parseFloat(boxHeight) + borderHeight;
          if (borderRadiusValue > 0) {
              $('#fancybox-bg-w, #fancybox-bg-nw, #fancybox-bg-sw, #fancybox-bg-s, #fancybox-bg-se, #fancybox-bg-e, #fancybox-bg-ne, #fancybox-bg-n').css('display', 'none');
              $('#fancybox-outer').css('background-color', 'transparent')
          }
          $(buttonToTriggerForm).attr("href", boxContainingForm);
          $(boxContainingForm).css({
              'top': '0',
              'left': '0',
          });
          $(buttonToTriggerForm).fancybox({
              'autoDimensions': false,
              'width': boxWidth,
              'height': boxHeight,
              'autoScale': false,
              'padding': 0,
              'margin': 0,
              'onStart': function() {
                  $(boxContainingForm).css('display', 'table')
              },
              'onComplete': function() {
                  $(boxContainingForm).parent().css('display', 'table')
              },
              'onClosed': function() {
                  $(boxContainingForm).css('display', 'none')
              }
          });
          $(boxContainingForm).addClass('lp-pom-root')
      })
  }
  $(function() {
      function triggerFormEvent() {
          if (lp.jQuery('form').valid() == true) {
              $.fancybox.close()
          }
      }
      $('.lp-pom-form .lp-pom-button').click(function() {
          triggerFormEvent()
      });
      $('.lp-pom-form').keypress(function(event) {
          if (event.which == 13) {
              event.preventDefault();
              triggerFormEvent()
          }
      })
  });