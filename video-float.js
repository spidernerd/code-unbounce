// Script que pega o video que esta rodando na pagina e coloca ele position fixed na pagina inferior 
// direita ou aonde a pessoa preferir, só editando o .css()  

$(document).ready(function () {
    if ($(window).width() > 1200) {

        var idVideo = $('#lp-pom-video-414'),
            largura = $(window).width(),
            altura = $(window).height(),
            larguraMargin = largura - 310,
            alturaMargin = altura - 180,
            wigthVideo = 300,
            heigthVideo = 170,
            borderSize = 5;

        $(window).scroll(function () {
            if ($(this).scrollTop() > 800) {
                $('#lp-pom-video-414').addClass('float');
                $('#lp-pom-video-414').css({
                    'left': larguraMargin,
                    'top': alturaMargin,
                    wigth: wigthVideo,
                    height: heigthVideo,
                    'postion': 'fixed!important',
                    'border': borderSize + "px solid #002BFF"
                });
            } else {
                $('#lp-pom-video-414').removeClass('float');
                $('#lp-pom-video-414').removeAttr('style');
            }
        });
    }
});